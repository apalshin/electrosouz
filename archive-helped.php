<?php

get_header(); ?>

<div class="row">
    <div class="col-md-9">
        <section id="primary" class="content-area archive-feed leaflet-feed">
            <main id="main" class="site-main" role="main">

			    <?php if ( have_posts() ) : ?>

                    <header>
                        <h1 class="archive-title"><?php _e('Профсоюз помог'); ?></h1>
                    </header><!-- .page-header -->

                    <figure class="leaflet-grid">

					    <div class="row">

                        <?php
                        // Start the Loop.
                        while ( have_posts() ) : the_post(); ?>

	                        <?php
	                        $uploaded_file = get_post_meta( $post->ID, '_wp_helped_pdf_attachment' );
	                        $uploaded_file = $uploaded_file[0]['url'];
	                        $thumbnail = get_the_post_thumbnail_url();
	                        if (empty($thumbnail)) $thumbnail = get_theme_file_uri('img/pdf_file_icon.png');
	                        ?>

	                        <div class="col-md-6">
		                        <a href="<?php echo $uploaded_file ?>" target="_blank">
			                        <section id="post-<?php the_ID(); ?>" class="archive-item leaflet-item">
				                        <div class="row row-flex">
					                        <div class="col-xs-4">
						                        <figure class="pdf-file-icon">
							                        <picture>
								                        <img src="<?php echo $thumbnail ?>" class="img-responsive">
							                        </picture>
						                        </figure>
					                        </div>
					                        <div class="col-xs-8 pdf-column">
						                        <div class="archive-item-title">
							                        <?php the_title(); ?>
						                        </div>
						                        <div class="downnload-pdf">
							                        <button class="btn gold-button pdf-button"><?php _e('Открыть PDF'); ?></button>
						                        </div>
					                        </div>
				                        </div>
			                        </section>
		                        </a>

	                        </div>
                            <?php
                            // End the loop.
                        endwhile; ?>
					    </div>

                    </figure>


<?php

				    electro_numeric_posts_nav();

			    // If no content, include the "No posts found" template.
			    else :
				    get_template_part( 'content', 'none' );

			    endif;
			    ?>

            </main><!-- .site-main -->
        </section><!-- .content-area -->
    </div>
    <div class="col-md-3">
	    <?php get_sidebar(); ?>
    </div>
</div>

<?php get_footer(); ?>
